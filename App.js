import React from 'react'
import { View } from 'react-native'

import Tom from "./screens/tom"

const App = () => {
  return (
    <View>
      
      <Tom />
    </View>
  )
}

export default App
