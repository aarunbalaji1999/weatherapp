import React  , {useState,useEffect}from 'react'
import { View, Text,Image, TouchableOpacity } from 'react-native'
import { TextInput,Card,Title} from 'react-native-paper';

import moment from 'moment'
import Ionicons from 'react-native-vector-icons/Ionicons';


const tom =()=>{
    const [city,setCity]=useState("")
    const[info,setInfo]=useState({
        name:"loading",
        temp:"loading",
        desc:"loading",
        icon:"loading"

    })
    useEffect(()=>{
        getc()
     },[])
     
   
   var date = moment(new Date());
  var weekDayName = moment(date).format("dddd");
//   alert(weekDayName)
//   alert(date.format("DD-MMMM-YYYY"))
    
  
    //  console.log(date.format("DD-MMMM-YYYY"))
     


   const getc = (data) =>
   {
       console.log(data)
     fetch('https://api.openweathermap.org/data/2.5/weather?q=' + data +'&APPID=ec53e02b6fe4813c7d8ffa0a723d46f6&units=metric', {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },

     }).then((response) => response.json())
        .then((responseJson) => {
            console.log(JSON.stringify(responseJson))
            setInfo({
                name:responseJson.name,
                temp:responseJson.main.temp,
                humidity:responseJson.main.humidity,
                desc:responseJson.weather[0].description,
                icon:responseJson.weather[0].icon,
            })
            console.log(JSON.stringify(responseJson))
          
        }).catch((error) => {
           console.log(error+"***")
        });}

    
          
        
    
  
      return (
        <View style={{padding:10,backgroundColor:"#008b8b"}}>
             <TextInput label="city name" 
                    theme={{colors:{primary:"#008b8b"}}}
                
                    value={city}
                    onChangeText={(text)=>{setCity(text)
                        getc(city)}

                    }
                    >
                    
                    </TextInput>
                 <View >
                    <View style={{flexDirection:"row",alignContent:"center"}}>
                        <View style={{width:"50%"}}>
                            <Text style={{fontSize:20 ,fontWeight:"500",color:"black"}}>{weekDayName}, {info.name}</Text>
                           <Text style={{fontSize:50,color:"black"}}>{ info.temp}°C</Text>
                   <Text style={{color:"black",borderBottomWidth: 1,borderTopWidth:1,fontSize:20,fontWeight:"bold"}}>{date.format("DD-MMMM-YYYY")}</Text>
                   </View>
                   <View style={{width:"50%"}}>
                       <View style={{alignSelf:"flex-end"}}>
                       <TouchableOpacity  onpress={()=>getc()}>
                           
                       <Ionicons
                     
                           name="refresh"
                            color="black"
                            size={24}
                            
                            style={{backgroundColor: 'transparent'}}
                              />
                             
                       </TouchableOpacity>
                       </View>
                       <View style={{alignSelf:"flex-end"}}>
                       <Image 
                          style={{
                       width:120,
                       height:140,
                      
                      
                      
                      
                         }}
                           source={{uri:"https://openweathermap.org/img/w/"+info.icon+".png"}}
                   
                       />
                       <Title style={{color:"black",borderBottomWidth: 1,borderTopWidth:1}}>  {info.desc}</Title>
                       </View>
                    </View>
                   </View>
                  
                 
                   
                  
                   <View >
                   <Card style={{
                            margin:5,
                            padding:12,
                            backgroundColor:"#008b8b"
                            
                                    }}>
                                <Title style={{color:"black"}}>Friday               18 C             cloudy</Title>
                      </Card>
                      <Card style={{
                            margin:5,
                            padding:12,
                            backgroundColor:"#008b8b"
                                    }}>
                                <Title style={{color:"black"}}>Saturday           20 C               Rain</Title>
                      </Card>
                      <Card style={{
                            margin:5,
                            padding:12,
                            backgroundColor:"#008b8b"
                            
                                    }}>
                                <Title style={{color:"black"}}>Sunday              25 C             haze</Title>
                      </Card>
                      <Card style={{
                            margin:5,
                            padding:12,
                            backgroundColor:"#008b8b"
                                    }}>
                                <Title style={{color:"black"}}>Monday            22 C              mist</Title>
                      </Card>
                      <Card style={{
                            margin:5,
                            padding:12,
                            backgroundColor:"#008b8b"
                                    }}>
                                <Title style={{color:"black"}}>Tuesday           18 C            cloudy</Title>
                      </Card>


                     
                   </View>
    
               </View>
              
           
        </View>
      )
   }

export default tom

